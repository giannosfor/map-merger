package com.acme;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapUtils {

    public static BinaryOperator<Integer> accumulator = (a, b) -> a + b;
    public static BinaryOperator<Integer> multiplier = (a, b) -> a * b;
    public static BinaryOperator<String> concatenate = (a,b) -> a + b;

    public static <I,T> Map<I,T> merge(BinaryOperator<T> bOper, Map<I,T> mapA, Map<I,T> mapB) {
        return Stream.of(mapA, mapB)
                .filter(Objects::nonNull)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .filter(mapEntry -> mapEntry.getKey() != null && mapEntry.getValue() != null)
                .collect( Collectors.toMap( Map.Entry::getKey, Map.Entry::getValue, bOper ) );
    }

    public static <I,T> Map<I,T> mergeListOfMaps(BinaryOperator<T> bOper, List<Map<I,T>> mapList) {
        return mapList
                .parallelStream()
                .filter(Objects::nonNull)
                .reduce( (m1, m2) -> merge(bOper, m1, m2) )
                .orElseGet(Collections::emptyMap);
    }

}

package com.acme;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapUtilsTest {
    private Map mi1 = ImmutableMap.of("a", 1, "b", 3);
    private Map mi2 = ImmutableMap.of("a", 1, "b", 3, "c", 4, "d", 5);
    private Map mi3 = ImmutableMap.of("a", 3, "b", 4, "c", 2);

    private List<Map<String, Integer>> listOfIntegerMaps = new ArrayList() {{ add(mi1); add(mi2); add(mi3); }};

    private Map ms1 = ImmutableMap.of("a", "test", "b", "fix");
    private Map ms2 = ImmutableMap.of("a", "it", "b", "it", "c", "bug");
    private Map ms3 = ImmutableMap.of("a", "on", "b", "then", "c", "proof");

    private List<Map<String, String>> listOfStringMaps = new ArrayList() {{ add(ms1); add(ms2); add(ms3); }};

    @Before
    public void initilizeMaps() {
    }

    @Test
    public void mergeWithStringConcatenationBehaviour() {
        Map<String, String> msConc = ImmutableMap.of("a", "testit", "b", "fixit", "c", "bug");

        Map<String, String> mMerg = MapUtils.merge(MapUtils.concatenate, ms1, ms2);
        Assert.assertTrue(mMerg.equals(msConc));
    }

    @Test
    public void mergeWithAccumulationBehaviour() {
        Map<String, Integer> mAcc = ImmutableMap.of("a", 2, "b", 6, "c", 4, "d", 5);

        Map<String, Integer> mMerg = MapUtils.merge(MapUtils.accumulator, mi1, mi2);
        Assert.assertTrue(mMerg.equals(mAcc));
    }

    @Test
    public void mergeWithMultiplyBehaviour() {
        Map<String, Integer> mMult = ImmutableMap.of("a", 1, "b", 9, "c", 4, "d", 5);

        Map<String, Integer> mMerg = MapUtils.merge(MapUtils.multiplier, mi1, mi2);
        Assert.assertTrue(mMerg.equals(mMult));
    }

    @Test
    public void testMergeListOfMapsWithAdditionBehaviour() {
        Map<String, Integer> mAdd = ImmutableMap.of("a", 5, "b", 10, "c", 6, "d", 5);

        Map<String, Integer> mMerg = MapUtils.mergeListOfMaps(MapUtils.accumulator, listOfIntegerMaps);
        Assert.assertTrue(mAdd.equals(mMerg));
    }

    @Test
    public void testMergeListOfStringMapsWithConcatBehaviour() {
        Map<String, String> msConc = ImmutableMap.of("a", "testiton", "b", "fixitthen", "c", "bugproof");

        Map<String, String> mMerg = MapUtils.mergeListOfMaps(MapUtils.concatenate, listOfStringMaps);
        Assert.assertTrue(msConc.equals(mMerg));
    }

    @Test
    public void testListOfMapsWithNullEntry() {
        List<Map<String, Integer>> listOfIntegerMapsWithNull = new ArrayList() {{ add(mi1); add(null); add(mi3); }};
        Map<String, Integer> expectedMap = ImmutableMap.of("a", 4, "b", 7, "c", 2);

        Map<String, Integer> mergedMap = MapUtils.mergeListOfMaps(MapUtils.accumulator, listOfIntegerMapsWithNull);
        Assert.assertNotNull(mergedMap);
        Assert.assertNotNull(expectedMap);
        Assert.assertEquals(expectedMap, mergedMap);
    }

    @Test
    public void testMapWithNullEntry() {

        Map<String,Integer> mapWithNullKeyOrValue = new HashMap(){{ put("a",null); put(null,1); put(null,null); put("d",4);  }};

        List<Map<String, Integer>> listOfIntegerMapsWithNull = new ArrayList() {{ add(mi1); add(mapWithNullKeyOrValue); add(mi3); }};
        Map<String, Integer> expectedMap = ImmutableMap.of("a", 4, "b", 7, "c", 2, "d", 4);

        Map<String, Integer> mergedMap = MapUtils.mergeListOfMaps(MapUtils.accumulator, listOfIntegerMapsWithNull);
        Assert.assertNotNull(mergedMap);
        Assert.assertNotNull(expectedMap);
        Assert.assertEquals(expectedMap, mergedMap);
    }
}

// Notes:
//
//        mergedMap.forEach((key, value) -> System.out.println(key + " : " + value));
//        expectedMap.forEach((key, value) -> System.out.println(key + " : " + value));
